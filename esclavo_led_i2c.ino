#include <Wire.h> // <-- Biblioteca I2C
int LED = 9;
int x = 0;
void setup()
{
// Define el pin 13 como salida
pinMode (LED, OUTPUT);
// Inicia el bus I2C como esclavo en la dirección 9
Wire.begin(9);
// Agrega una función para se dispara cuando se reciba algo.
Wire.onReceive(receiveEvent);
}

void receiveEvent( int bytes )
{
x = Wire.read(); // Lee un carácter de I2C
  Serial.print(x,DEC);
}

void loop()
{
if (x == 1) {
digitalWrite(LED, HIGH);
delay(1000);
}
if (x == 0) {
digitalWrite(LED, LOW);
delay(1000);
}
} 