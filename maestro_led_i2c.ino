#include <Wire.h> // <-- Biblioteca I2C
int x =0;
int slaveAddress = 9;
const int buttonPin = 2; // pin digital a usar
int buttonState = 0; // variable para la lectura del pushbuton

void setup()
{
pinMode( buttonPin, INPUT ); // Inicializa el pin 2 para lectura
Wire.begin(); // se conecta al bus I2C como dispositivo maestro
Serial.begin( 9600 ); // Inicia velocidad en baud de comunicación serial
}
void loop()
{
// Lee el valor actual del pushbutton
buttonState = digitalRead( buttonPin );
// verifica si el pushbutton se ha presionado. Si es así, la variable buttonState es HIGH:
if ( buttonState == HIGH )
{
x = 1;
  Serial.println("ALTO");
}
else
{
x = 0;
  Serial.println("BAJO");
}
Wire.beginTransmission(slaveAddress); // transmite el dato al dispositivo esclavo #9
Wire.write(x); // envía el valor de x
Wire.endTransmission(); // detiene la transmisión
delay(200);
}